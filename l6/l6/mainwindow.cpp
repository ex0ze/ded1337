#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , pWidget(new PainterWidget(this))
{
    ui->setupUi(this);
    ui->pWidget_layout->addWidget(pWidget);
    connect(ui->actionClear, &QAction::triggered, pWidget, &PainterWidget::clearDrawables);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btn_triangle_clicked()
{
    TriangleDrawable* triangle = new TriangleDrawable(nullptr);
    //QPointF a(35,50), b(50,90), c(90,70);
    QPointF a(qrand()%100,qrand()%100), b(qrand()%100,qrand()%100), c(qrand()%100,qrand()%100);
    triangle->setPoints(a,b,c);
    pWidget->pushDrawable(triangle);
}

void MainWindow::on_btn_isoscelesTriangle_clicked()
{
    IsoscelesTriangleDrawable* triangle = new IsoscelesTriangleDrawable(nullptr);
    triangle->setGeometry(QPointF(50,20), 100,70);
    pWidget->pushDrawable(triangle);
}

void MainWindow::on_btn_equilateraltriangle_clicked()
{
    EquilateralTriangleDrawable* triangle = new EquilateralTriangleDrawable(nullptr);
    triangle->setGeometry(QPointF(50,20), 70);
    pWidget->pushDrawable(triangle);
}
