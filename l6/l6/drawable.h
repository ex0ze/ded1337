#ifndef IDRAWABLE_H
#define IDRAWABLE_H

#include <QObject>
#include <QWidget>
#include <QMap>
#include <QPoint>
#include <QPainter>
#include <QSharedPointer>
#include <cmath>

class IDrawable
{
public:
    virtual void draw(const QPointF& location) = 0;
    virtual void resetPainter(QPainter* _newPainter = nullptr);
protected:
    explicit IDrawable(QPainter* p);
    QPainter* m_painterPtr;
};

class TriangleDrawable : public IDrawable
{
public:
    explicit TriangleDrawable(QPainter* p);
    virtual void draw(const QPointF &location) override;
    void setPoints(const QPointF& _a, const QPointF& _b, const QPointF& _c);
protected:
    QPointF m_points[3];
};

class IsoscelesTriangleDrawable : public TriangleDrawable
{
public:
    explicit IsoscelesTriangleDrawable(QPainter* p);
    void setGeometry(const QPointF& _a, qreal side_len, qreal base_len);
};

class EquilateralTriangleDrawable : public IsoscelesTriangleDrawable
{
public:
    explicit EquilateralTriangleDrawable(QPainter* p);
    void setGeometry(const QPointF& _a, qreal side_len);
};

class PainterWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PainterWidget(QWidget* parent = nullptr);
    ~PainterWidget();
public slots:
    void pushDrawable(IDrawable* d);
    void clearDrawables();
protected:
    void paintEvent(QPaintEvent *event) override;
private:
    QVector<QSharedPointer<IDrawable>> m_drawables;
    const QMap<int,QPointF> m_positions = {{0, {100,100}},
                                           {1, {300,100}},
                                           {2, {500,100}}};
    int m_currentDrawable;
};

#endif // IDRAWABLE_H
