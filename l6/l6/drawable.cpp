#include "drawable.h"

IDrawable::IDrawable(QPainter *p) : m_painterPtr(p)
{

}

void IDrawable::resetPainter(QPainter *_newPainter)
{
    m_painterPtr = _newPainter;
}

TriangleDrawable::TriangleDrawable(QPainter *p) : IDrawable(p)
{

}

void TriangleDrawable::draw(const QPointF &location)
{
    if (!m_painterPtr) throw std::runtime_error("IDrawable nullptr exception");
    QPointF offset = location - m_points[0];
    QPointF _points[3];
    for (int i=0;i<3;++i) _points[i] = m_points[i] + offset;
    m_painterPtr->drawPolygon(_points, 3);
}

void TriangleDrawable::setPoints(const QPointF &_a, const QPointF &_b, const QPointF &_c)
{
    m_points[0] = _a;
    m_points[1] = _b;
    m_points[2] = _c;
}

PainterWidget::PainterWidget(QWidget *parent) : QWidget(parent), m_currentDrawable(0)
{
    m_drawables.resize(3);
    setFixedSize(600,200);
}

PainterWidget::~PainterWidget()
{
}

void PainterWidget::pushDrawable(IDrawable *d)
{
    m_drawables[m_currentDrawable++].reset(d);
    m_currentDrawable %= 3;
    QWidget::repaint();
}

void PainterWidget::clearDrawables()
{
    for (auto &p : m_drawables) p.reset();
    QWidget::repaint();
}

void PainterWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));
    for (int i = 0; i < 3;++i) {
        if (m_drawables[i] != nullptr) {
            m_drawables[i]->resetPainter(&painter);
            m_drawables[i]->draw(m_positions[i]);
        }
    }
}

IsoscelesTriangleDrawable::IsoscelesTriangleDrawable(QPainter *p) : TriangleDrawable(p)
{

}

void IsoscelesTriangleDrawable::setGeometry(const QPointF &_a, qreal side_len, qreal base_len)
{
    qreal height = std::sqrt(side_len*side_len - (base_len/2)*(base_len/2));
    QPointF height_base(_a.x(), _a.y() + height);
    m_points[0] = _a;
    m_points[1] = QPointF(height_base.x() - base_len/2, height_base.y());
    m_points[2] = QPointF(height_base.x() + base_len/2, height_base.y());
}

EquilateralTriangleDrawable::EquilateralTriangleDrawable(QPainter *p) : IsoscelesTriangleDrawable(p)
{

}

void EquilateralTriangleDrawable::setGeometry(const QPointF &_a, qreal side_len)
{
    IsoscelesTriangleDrawable::setGeometry(_a, side_len, side_len);
}
