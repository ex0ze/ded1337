#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "drawable.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_triangle_clicked();

    void on_btn_isoscelesTriangle_clicked();

    void on_btn_equilateraltriangle_clicked();

private:
    Ui::MainWindow *ui;
    PainterWidget* pWidget;
};
#endif // MAINWINDOW_H
