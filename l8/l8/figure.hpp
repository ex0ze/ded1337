#ifndef FIGURE_HPP
#define FIGURE_HPP

#include <cmath>
#include <array>
#include <iostream>

//#define _FIGURE_DEBUG

template <class Ty>
struct Point2 {
    Ty x;
    Ty y;
    Point2(Ty _x, Ty _y) : x(_x), y(_y) {}
    Point2() : x(Ty()), y(Ty()) {}
};

template<typename Ty1, typename Ty2>
constexpr bool isEqual(Ty1 a, Ty2 b, double eps)
{
    return std::fabs(a-b) < eps;
}

template <class Ty>
static Ty distance(const Point2<Ty>& _a, const Point2<Ty>& _b)
{
    Ty _xdiff = (_b.x - _a.x) * (_b.x - _a.x);
    Ty _ydiff = (_b.y - _a.y) * (_b.y - _a.y);
    return std::sqrt(_xdiff + _ydiff);
}

template <class Ty>
static bool isParallel(const Point2<Ty>& beg1, const Point2<Ty>& end1, const Point2<Ty>& beg2, const Point2<Ty>& end2)
{
    Ty slope_1 = (end1.y - beg1.y) / (end1.x - beg1.x);
    Ty slope_2 = (end2.y - beg2.y) / (end2.x - beg2.x);
    return slope_1 == slope_2;
}

using Point2f = Point2<float>;
using Point2d = Point2<double>;

template <class PointType, std::size_t numVertices>
class Polygon
{
public:
    virtual PointType getSquare() const = 0;
    virtual PointType getPerimeter() const
    {
        PointType ret = PointType();
        for (std::size_t i = 0; i < numVertices - 1; ++i) {
            ret += getDistance(i, i+1);
        }
        ret += getDistance(numVertices - 1, 0);
        return ret;
    }
    std::array<Point2<PointType>, numVertices>& getVertices()
    {
        return m_vertices;
    }
    virtual void setVertices(const std::array<Point2<PointType>, numVertices>& _v)
    {
        m_vertices = _v;
    }
    Point2<PointType>& vertexAt(std::size_t idx)
    {
        if (idx < 0 || idx >= numVertices) throw "Undefined index exception";
        return m_vertices[idx];
    }
    PointType getDistance(std::size_t v1, std::size_t v2) const
    {
        if ((v1 < 0 || v1 >= numVertices) || (v2 < 0 && v2 >= numVertices)) throw "Undefined index exception";
        return distance(m_vertices[v1], m_vertices[v2]);
    }

protected:
    Polygon()
    {
#ifdef _FIGURE_DEBUG
        std::cout<<"Polygon constructed\n";
#endif
    }
    std::array<Point2<PointType>, numVertices> m_vertices;
};

template <class PointType>
class Triangle : public Polygon<PointType, 3ull>
{
public:
    Triangle() : Polygon<PointType, 3ull>()
    {
#ifdef _FIGURE_DEBUG
        std::cout<<"Triangle constructed\n";
#endif
    }
    virtual PointType getSquare() const override
    {
        PointType m1, m2, m3, m4, det;
        m1 = this->m_vertices[0].x - this->m_vertices[2].x;
        m2 = this->m_vertices[0].y - this->m_vertices[2].y;
        m3 = this->m_vertices[1].x - this->m_vertices[2].x;
        m4 = this->m_vertices[1].y - this->m_vertices[2].y;
        det = m1*m4 - m2*m3;
        return std::fabs(det/2.0);
    }
};

template <class PointType>
class Quadrangle : public Polygon<PointType, 4ull>
{
public:
    Quadrangle() : Polygon<PointType, 4ull> ()
    {
#ifdef _FIGURE_DEBUG
        std::cout<<"Quadrangle constructed\n";
#endif
    }
    virtual PointType getSquare() const override
    {
        auto perimeter = Polygon<PointType, 4ull>::getPerimeter() / 2.0;
        return std::sqrt((perimeter - this->getDistance(0,1)) *
                         (perimeter - this->getDistance(1,2)) *
                         (perimeter - this->getDistance(2,3)) *
                         (perimeter - this->getDistance(3,0)));

    }
};

template <class PointType>
class Parallelogram : public Quadrangle<PointType>
{
public:
    Parallelogram() : Quadrangle<PointType>()
    {
#ifdef _FIGURE_DEBUG
        std::cout<<"Parallelogram constructed\n";
#endif
    }
    virtual void setVertices(const std::array<Point2<PointType>, 4ull>& _v)
    {
        Polygon<PointType, 4>::setVertices(_v);
        if (!isValid()) throw "Invalid Parallelogram";
    }
protected:
    virtual bool isValid()
    {
        return (isParallel(this->m_vertices[0], this->m_vertices[1], this->m_vertices[3], this->m_vertices[2])) &&
                (isParallel(this->m_vertices[1], this->m_vertices[2], this->m_vertices[0], this->m_vertices[3]));
    }
};

template <class PointType>
class Rectangle : public virtual Parallelogram<PointType>
{
public:
    Rectangle() : Parallelogram<PointType>()
    {
#ifdef _FIGURE_DEBUG
        std::cout<<"Rectangle constructed\n";
#endif
    }
    virtual void setVertices(const std::array<Point2<PointType>, 4ull>& _v)
    {
        Parallelogram<PointType>::setVertices(_v);
        if (!isValid()) throw "Invalid Rectangle";
    }
protected:
    virtual bool isValid()
    {
        PointType vec1_x, vec1_y, vec2_x, vec2_y, vec3_x, vec3_y, vec4_x, vec4_y;
        vec1_x = this->m_vertices[1].x - this->m_vertices[0].x;
        vec1_y = this->m_vertices[1].y - this->m_vertices[0].y;
        vec2_x = this->m_vertices[3].x - this->m_vertices[0].x;
        vec2_y = this->m_vertices[3].y - this->m_vertices[0].y;

        vec3_x = this->m_vertices[2].x - this->m_vertices[1].x;
        vec3_y = this->m_vertices[2].y - this->m_vertices[1].y;
        vec4_x = this->m_vertices[3].x - this->m_vertices[2].x;
        vec4_y = this->m_vertices[3].y - this->m_vertices[2].y;

        PointType scalar1, scalar2;
        scalar1 = vec1_x*vec2_x + vec1_y*vec2_y;
        scalar2 = vec3_x*vec4_x + vec3_y*vec4_y;
        return (scalar1 == 0 && scalar2 == 0);

    }
};

template <class PointType>
class Rhombus : public virtual Parallelogram<PointType>
{
public:
    Rhombus() : Parallelogram<PointType>()
    {
#ifdef _FIGURE_DEBUG
        std::cout<<"Rhombus constructed\n";
#endif
    }
    virtual void setVertices(const std::array<Point2<PointType>, 4ull>& _v)
    {
        Parallelogram<PointType>::setVertices(_v);
        if (!isValid()) throw "Invalid Rhombus";
    }
protected:
    virtual bool isValid()
    {
        auto d1 = distance(this->m_vertices[0],this->m_vertices[1]);
        auto d2 = distance(this->m_vertices[1],this->m_vertices[2]);
        auto d3 = distance(this->m_vertices[2],this->m_vertices[3]);
        auto d4 = distance(this->m_vertices[3],this->m_vertices[0]);
        return isEqual(d1,d2,0.0001) &&
                isEqual(d2,d3,0.0001) &&
                isEqual(d3,d4,0.0001);
    }
};

template <class PointType>
class Square : public Rectangle<PointType>, public Rhombus<PointType>
{
public:
    Square() : Rectangle<PointType>(), Rhombus<PointType>()
    {
#ifdef _FIGURE_DEBUG
        std::cout<<"Square constructed\n";
#endif
    }
    virtual void setVertices(const std::array<Point2<PointType>, 4ull>& _v)
    {
        Parallelogram<PointType>::setVertices(_v);
        if (!isValid()) throw "Invalid Square";
    }
protected:
    virtual bool isValid()
    {
        return Rectangle<PointType>::isValid() && Rhombus<PointType>::isValid();
    }
};

#endif // POLYGON_H

