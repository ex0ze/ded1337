#include <iostream>
#include <memory>
#include "figure.hpp"

template <class Ty, std::size_t numVertices>
void test_obj(Polygon<Ty, numVertices>* poly) {
    std::cout<<poly->getSquare();
    std::cout<<"\t";
    std::cout<<poly->getPerimeter();
    std::cout<<std::endl;
}

int main()
{
    //Quadrangle<double> quad;
    auto quad = std::make_unique<Quadrangle<double>>();
    quad->setVertices({{{0.0,0.0}, {5.0,15.0}, {10.0,10.0}, {3.0,-5.0}}});

    auto tri = std::make_unique<Triangle<double>>();
    tri->setVertices({{{3.0,2.0}, {7.0,5.0}, {0.0,0.0}}});

    auto par = std::make_unique<Parallelogram<double>>();
    par->setVertices({{{-5.0,0.0}, {0.0,5.0}, {5.0,5.0}, {0.0,0.0}}});

    auto rho = std::make_unique<Rhombus<double>>();
    rho->setVertices({{{0.0,0.0}, {-2.0,5.0}, {0.0,10.0}, {2.0,5.0}}});

    auto rect = std::make_unique<Rectangle<double>>();
    rect->setVertices({{{0.0,0.0}, {0.0,5.0}, {10.0,5.0}, {10.0,0.0}}});

    auto sq = std::make_unique<Square<double>>();
    sq->setVertices({{{0.0,0.0}, {0.0,5.0}, {5.0,5.0}, {5.0,0.0}}});

    std::cout<<"Square\tPerimeter\n";
    test_obj(quad.get());
    test_obj(tri.get());
    test_obj(par.get());
    test_obj(rho.get());
    test_obj(rect.get());
    test_obj(sq.get());
    return 0;
}
