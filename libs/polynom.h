#ifndef POLYNOM_H
#define POLYNOM_H

#include <cmath>
#include "../../libs/__tcomplex.h"
#include "__number.h"
class polynom
{
public:
    polynom() : m_a(0), m_b(0), m_c(0)
    {

    }
    polynom(number a, number b, number c) : m_a(a), m_b(b), m_c(c)
    {

    }
    int solve(number* roots)
    {
        number sqrt_d = sqrt(m_b * m_b - 4.0 * m_a * m_c);
        if (sqrt_d < 0) return 0;
        else {
            roots[0] = (-m_b - sqrt_d) / (2.0 * m_a);
            roots[1] = (-m_b + sqrt_d) / (2.0 * m_a);
            return 2;
        }
    }
private:
    number m_a;
    number m_b;
    number m_c;
};

#endif // POLYNOM_H
