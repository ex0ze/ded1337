#ifndef POLYNOM_EXCHANGE_H
#define POLYNOM_EXCHANGE_H

#include <QObject>
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <iostream>
#include "polynom.h"
#include "polynom_dgram.h"

class IUdpExchange : public QObject
{
    Q_OBJECT
public:
    explicit IUdpExchange(QObject* parent = nullptr);
    ~IUdpExchange();
    bool running() const;

public slots:
    protected:
protected:
    bool m_running;
    QUdpSocket m_socket;
};

class PolynomialServer : public IUdpExchange
{
    Q_OBJECT
public:
    explicit PolynomialServer(QObject *parent = nullptr);
public slots:
    virtual bool start();
    virtual bool stop();
private slots:
    void handleConnection();
signals:


};

class PolynomialClient : public IUdpExchange
{
    Q_OBJECT
public:
    explicit PolynomialClient(QObject* parent = nullptr);
public slots:
    int sendPolynom(TComplex* poly, TComplex* roots);
};

#endif // POLYNOM_EXCHANGE_H
