#ifndef TCOMPLEX_H
#define TCOMPLEX_H

#include <cmath>
#include <iostream>

class TComplex
{
public:
    TComplex() : m_re(0), m_im(0)
    {
    }
    TComplex(double real, double imag) : m_re(real), m_im(imag)
    {

    }
    TComplex(double real) : m_re(real), m_im(0)
    {

    }
    bool operator<(double v) {
        return m_re < v;
    }
    bool operator>(double v) {
        return m_re > v;
    }
    TComplex& operator=(const TComplex& rhs)
    {
        m_re = rhs.m_re;
        m_im = rhs.m_im;
        return *this;
    }
    bool operator==(const TComplex& rhs)
    {
        return (this->m_im == rhs.m_im && this->m_re == rhs.m_re);
    }
    TComplex& operator=(const double& rhs)
    {
        this->m_re = rhs;
        this->m_im = double();
        return *this;
    }
    TComplex operator-()
    {
        return TComplex(-this->m_re,-this->m_im);
    }
    TComplex operator+(const TComplex& rhs)
    {
        TComplex _res = *this;
        _res += rhs;
        return _res;
    }
    TComplex& operator+=(const TComplex& rhs)
    {
        this->m_im += rhs.m_im;
        this->m_re += rhs.m_re;
        return *this;
    }
    TComplex operator-(const TComplex& rhs)
    {
        TComplex _res = *this;
        _res -= rhs;
        return _res;
    }
    TComplex& operator-=(const TComplex& rhs)
    {
        this->m_im -= rhs.m_im;
        this->m_re -= rhs.m_re;
        return *this;
    }
    TComplex operator*(const TComplex& rhs)
    {
        TComplex _res = *this;
        _res *= rhs;
        return _res;
    }
    TComplex& operator*=(const TComplex& rhs)
    {

        double _re = (this->m_re * rhs.m_re) - (this->m_im * rhs.m_im);
        double _im = (this->m_re * rhs.m_im + this->m_im * rhs.m_re);
        this->m_im = _im;
        this->m_re = _re;
        return *this;
    }
    TComplex operator/(const TComplex& rhs)
    {
        TComplex _res = *this;
        _res /= rhs;
        return _res;
    }
    TComplex& operator/=(const TComplex& rhs)
    {
        double _re = (this->m_re * rhs.m_re + this->m_im * rhs.m_im) / (rhs.m_re * rhs.m_re + rhs.m_im * rhs.m_im);
        double _im = (rhs.m_re * this->m_im - this->m_re * rhs.m_im) / (rhs.m_re * rhs.m_re + rhs.m_im * rhs.m_im);
        this->m_im = _im;
        this->m_re = _re;
        return *this;
    }
    TComplex& operator+=(const double& rhs)
    {
        this->m_re += rhs;
        return *this;
    }
    TComplex& operator-=(const double& rhs)
    {
        this->m_re -= rhs;
        return *this;
    }
    TComplex& operator*=(const double& rhs)
    {
        this->m_re *= rhs;
        this->m_im *= rhs;
        return *this;
    }
    TComplex& operator/=(const double& rhs)
    {
        this->m_re /= rhs;
        this->m_im /= rhs;
        return *this;
    }
    TComplex operator+(const double& rhs)
    {
        TComplex _res = *this;
        _res += rhs;
        return _res;
    }
    TComplex operator-(const double& rhs)
    {
        TComplex _res = *this;
        _res -= rhs;
        return _res;
    }
    TComplex operator*(const double& rhs)
    {
        TComplex _res = *this;
        _res *= rhs;
        return _res;
    }
    TComplex operator/(const double& rhs)
    {
        TComplex _res = *this;
        _res /= rhs;
        return _res;
    }
    ~TComplex() = default;
    double real()
    {
        return m_re;
    }
    double imag()
    {
        return m_im;
    }
    void real(double _real)
    {
        this->m_re = _real;
    }
    void imag(double _imag)
    {
        this->m_im = _imag;
    }
    friend std::ostream& operator<< (std::ostream &out, const TComplex &c)
    {
        out<<"( " << c.m_re << " , " << c.m_im << " )";
        return out;
    }
    friend std::istream& operator>>(std::istream& in, TComplex& c)
    {
        std::cout<<"Enter real part: ";
        in >> c.m_re;
        std::cout<<"Enter imag part: ";
        in >> c.m_im;
        return in;
    }
private:
    double m_re;
    double m_im;
};


inline TComplex operator+(const double& x, TComplex y)
{
    TComplex _res = y;
    _res += x;
    return _res;
}
inline TComplex operator-(const double& x, TComplex y)
{
    TComplex _res(x, -y.imag());
    _res -= y.real();
    return _res;
}
inline TComplex operator*(const double& x, TComplex y)
{
    TComplex _res = y;
    _res *= x;
    return _res;
}
inline TComplex operator/(const double& x, TComplex y)
{
    TComplex _res(x,0);
    _res /= y;
    return _res;
}

static TComplex sqrt(TComplex _val)
{
    double _a = _val.real();
    double _b = _val.imag();
    double sgn = (_b >= 0) ? 1.0 : -1.0;
    double _re = std::sqrt((std::sqrt(_a * _a + _b * _b) + _a) / 2.0);
    double _im = std::sqrt(sgn * (std::sqrt(_a * _a + _b * _b) - _a) / 2.0);
    return TComplex(_re, _im);
}

#endif // TCOMPLEX_H
