#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "figure_storage.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_add_clicked();

    void on_btn_change_size_clicked();

    void on_btn_calculate_clicked();

    void clear_figures();

    void clear_fields();

private:
    Ui::MainWindow *ui;
    figure_storage m_fstorage;
};
#endif // MAINWINDOW_H
