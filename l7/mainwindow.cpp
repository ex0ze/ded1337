#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow), m_fstorage(200,200)
{
    ui->setupUi(this);
    connect(ui->act_clear_figures, &QAction::triggered, this, &MainWindow::clear_figures);
    connect(ui->act_clear_params, &QAction::triggered, this, &MainWindow::clear_fields);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btn_add_clicked()
{
    bool task[4] = {0,0,0,0};
    if (ui->circle_rad->text().toDouble() != 0)
    {
        task[0] = true; //КРУГ
    }
    if (ui->ellipse_big->text().toDouble() != 0 || ui->ellipse_small->text().toDouble() != 0)
    {
        task[1] = true; //ЭЛЛИПС
    }
    if (ui->square_length->text().toDouble() != 0)
    {
        task[2] = true; //КВАДРАТ
    }
    if (ui->rect_width->text().toDouble() != 0 || ui->rect_height->text().toDouble() != 0)
    {
        task[3] = true; //ПРЯМОУГОЛЬНИК
    }
    int t_count = 0;
    int t_pos = 0;
    for (int i = 0; i < 4; i++)
    {
        if (task[i]) {

            t_count++;
            t_pos = i;
        }
    }
    if (t_count > 1 || t_count == 0)
    {
        QMessageBox::critical(this, "Ошибка", "Измените параметры только одной добавляемой фигуры, остальное оставьте 0");
        return;
    }
    switch (t_pos) {
    case 0: {
        auto ci = QSharedPointer<abstract_figure>(new circle(ui->point_x->value(),ui->point_y->value(),ui->circle_rad->value()));
        ui->figure_list->addItem(ci->toString());
        m_fstorage.add_figure(ci);
        break;
    }
    case 1: {
        auto el = QSharedPointer<abstract_figure>(new ellipse(ui->point_x->value(), ui->point_y->value(),
                                                              ui->ellipse_big->value(), ui->ellipse_small->value()));
        ui->figure_list->addItem(el->toString());
        m_fstorage.add_figure(el);

        break;
    }
    case 2: {
        auto sq = QSharedPointer<abstract_figure>(new square(ui->point_x->value(),ui->point_y->value(), ui->square_length->value()));
        ui->figure_list->addItem(sq->toString());
        m_fstorage.add_figure(sq);
        break;
    }
    case 3: {
        auto rect = QSharedPointer<abstract_figure>(new rectangle(ui->point_x->value(),ui->point_y->value(),
                                                                  ui->rect_width->value(), ui->rect_height->value()));
        ui->figure_list->addItem(rect->toString());
        m_fstorage.add_figure(rect);
        break;
    }
    }
}

void MainWindow::on_btn_change_size_clicked()
{
    m_fstorage.set_size(ui->field_width->value(), ui->field_height->value());
    QMessageBox::information(this, "ОК", "Размер поля установлен");
}

void MainWindow::on_btn_calculate_clicked()
{
    double percent;
    if (ui->method_box->currentIndex() == 0)
    {
        percent = m_fstorage.regularMethod(ui->method_i->value(), ui->method_j->value());
    }
    else {
        percent = m_fstorage.randomMethod(ui->method_i->value(), ui->method_j->value());
    }
    QMessageBox::information(this, "Результат расчёта", "Доля узлов, не входящих в фигуры, составляет " + QString::number(percent));
}

void MainWindow::clear_figures()
{
    ui->figure_list->clear();
    m_fstorage.clear_figures();
}

void MainWindow::clear_fields()
{
    ui->circle_rad->setValue(0);
    ui->ellipse_big->setValue(0);
    ui->ellipse_small->setValue(0);
    ui->square_length->setValue(0);
    ui->rect_width->setValue(0);
    ui->rect_height->setValue(0);
}
