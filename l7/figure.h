#ifndef ABSTRACT_FIGURE_H
#define ABSTRACT_FIGURE_H
#include <cmath>
#include <QString>

class abstract_figure
{
public:
    abstract_figure(double center_x, double center_y) : m_x(center_x), m_y(center_y) {}
    virtual ~abstract_figure() = default;
    virtual QString toString() const = 0;
    virtual bool dead_inside(double x, double y) const = 0;
protected:
    double m_x;
    double m_y;
};

class ellipse : public abstract_figure
{
public:
    ellipse(double center_x, double center_y, double ellipse_big, double ellipse_small) : abstract_figure(center_x, center_y),
        m_ellipse_hor(ellipse_big), m_ellipse_ver(ellipse_small) {}
    virtual QString toString() const override;
    virtual ~ellipse() = default;
    virtual bool dead_inside(double x, double y) const override;
protected:
    double m_ellipse_hor;
    double m_ellipse_ver;
};

class rectangle : public abstract_figure
{
public:
    rectangle(double center_x, double center_y, double width, double height) : abstract_figure(center_x, center_y),
    m_width(width), m_height(height) {}
    virtual QString toString() const override;
    virtual ~rectangle() = default;
    virtual bool dead_inside(double x, double y) const override;
protected:
    double m_width;
    double m_height;
};

class circle : public ellipse
{
public:
    circle(double center_x, double center_y, double radius) : ellipse(center_x, center_y, radius, radius) {}
    virtual QString toString() const override;
    virtual ~circle() = default;
};

class square : public rectangle
{
public:
    square(double center_x, double center_y, double length) : rectangle(center_x, center_y, length, length) {}
    virtual QString toString() const override;
    virtual ~square() = default;
};

#endif // ABSTRACT_FIGURE_H
