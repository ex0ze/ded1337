#include "figure_storage.h"

figure_storage::figure_storage(double w, double h) : m_width(w), m_height(h)
{

}

void figure_storage::set_size(double w, double h)
{
    m_width = w;
    m_height = h;
}

void figure_storage::add_figure(QSharedPointer<abstract_figure> figure)
{
    m_figures.push_back(figure);
}

void figure_storage::clear_figures()
{
    m_figures.clear();
}

double figure_storage::regularMethod(double i, double j)
{
    for (double a = 0; a <= m_height; a += m_height / (double)i)
    {
        for (double b = 0; b <= m_width; b += m_width / (double)j)
        {
            m_points.push_back({b, a});
        }
    }
    double result = percentOutside();
    m_points.clear();
    return result;
}

double figure_storage::randomMethod(double i, double j)
{
    double rand_x, rand_y;
    std::default_random_engine generator;
    std::uniform_real_distribution<double> h_distribution(0, m_height);
    std::uniform_real_distribution<double> w_distribution(0, m_width);
    for (int a = 0; a < i*j; a++)
    {
        rand_x = w_distribution(generator);
        rand_y = h_distribution(generator);
        m_points.push_back({rand_x, rand_y});
    }
    double result = percentOutside();
    m_points.clear();
    return result;
}

double figure_storage::percentOutside() const
{
    int outside_count = m_points.count();
    for (const auto & point : m_points)
    {
        for (const auto & figure : m_figures)
        {
            if (figure->dead_inside(point.x(), point.y())) {
                outside_count--;
                break;
            }
        }
    }
    double points_count = m_points.count();
    return outside_count / points_count;
}
