#include "figure.h"

QString ellipse::toString() const
{
    return QString("Эллипс: Центр (%1,%2), Полуось_x %3, Полуось_y %4").arg(m_x).arg(m_y).arg(m_ellipse_hor).arg(m_ellipse_ver);
}

bool ellipse::dead_inside(double x, double y) const
{
    return ((std::pow((x - m_x),2) / std::pow(m_ellipse_hor,2) + std::pow((y - m_y),2) / std::pow(m_ellipse_ver,2)) <= 1.0);
}

QString rectangle::toString() const
{
    return QString("Прямоугольник: Центр (%1,%2), Ширина %3, Высота %4").arg(m_x).arg(m_y).arg(m_width).arg(m_height);
}

bool rectangle::dead_inside(double x, double y) const
{
    return ((x >= m_x - m_width/2.0) && (x <= m_x + m_width/2.0) && (y >= m_y - m_height/2.0) && (y <= m_y + m_height/2.0));
}

QString circle::toString() const
{
    return QString("Круг: Центр (%1,%2), Радиус %3").arg(m_x).arg(m_y).arg(m_ellipse_hor);
}

QString square::toString() const
{
    return QString("Квадрат: Центр (%1,%2), Сторона %3").arg(m_x).arg(m_y).arg(m_width);
}
