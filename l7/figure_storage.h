#ifndef FIGURE_STORAGE_H
#define FIGURE_STORAGE_H

#include <QList>
#include <QSharedPointer>
#include <random>
#include "figure.h"

class figure_storage
{
public:
    figure_storage(double w, double h);
    void set_size(double w, double h);
    void add_figure(QSharedPointer<abstract_figure> figure);
    void clear_figures();
    double regularMethod(double i, double j);
    double randomMethod(double i, double j);
private:
    double percentOutside() const;
    QList<QSharedPointer<abstract_figure>> m_figures;
    QList<QPointF> m_points;
    double m_width;
    double m_height;
};

#endif // FIGURE_STORAGE_H
