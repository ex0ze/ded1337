#ifndef POLYNOM_DGRAM_H
#define POLYNOM_DGRAM_H

#pragma pack(push, 1)

typedef struct {
    double a_re;
    double a_im;
    double b_re;
    double b_im;
    double c_re;
    double c_im;
} polynom_datagram_task_t;

typedef struct {
    double x1_re;
    double x1_im;
    double x2_re;
    double x2_im;
} polynom_datagram_result_t;

#pragma pack(pop)

#endif // POLYNOM_DGRAM_H
