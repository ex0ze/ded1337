#ifndef TCOMPLEX_H
#define TCOMPLEX_H

#include <cmath>
#include <ostream>

template<class T>
class TComplex
{
public:
    TComplex() : m_re(0), m_im(0)
    {
    }
    TComplex(T real, T imag) : m_re(real), m_im(imag)
    {

    }
    TComplex& operator=(const TComplex& rhs)
    {
        m_re = rhs.m_re;
        m_im = rhs.m_im;
        return *this;
    }
    bool operator==(const TComplex& rhs)
    {
        return (this->m_im == rhs.m_im && this->m_re == rhs.m_re);
    }
    template <class Tp>
    TComplex& operator=(const Tp& rhs)
    {
        this->m_re = rhs;
        this->m_im = Tp();
        return *this;
    }
    TComplex operator-()
    {
        return TComplex<T>(-this->m_re,-this->m_im);
    }
    TComplex operator+(const TComplex& rhs)
    {
        TComplex<T> _res = *this;
        _res += rhs;
        return _res;
    }
    TComplex& operator+=(const TComplex& rhs)
    {
        this->m_im += rhs.m_im;
        this->m_re += rhs.m_re;
        return *this;
    }
    TComplex operator-(const TComplex& rhs)
    {
        TComplex<T> _res = *this;
        _res -= rhs;
        return _res;
    }
    TComplex& operator-=(const TComplex& rhs)
    {
        this->m_im -= rhs.m_im;
        this->m_re -= rhs.m_re;
        return *this;
    }
    TComplex operator*(const TComplex& rhs)
    {
        TComplex<T> _res = *this;
        _res *= rhs;
        return _res;
    }
    TComplex& operator*=(const TComplex& rhs)
    {

        T _re = (this->m_re * rhs.m_re) - (this->m_im * rhs.m_im);
        T _im = (this->m_re * rhs.m_im + this->m_im * rhs.m_re);
        this->m_im = _im;
        this->m_re = _re;
        return *this;
    }
    TComplex operator/(const TComplex& rhs)
    {
        TComplex<T> _res = *this;
        _res /= rhs;
        return _res;
    }
    TComplex& operator/=(const TComplex& rhs)
    {
        T _re = (this->m_re * rhs.m_re + this->m_im * rhs.m_im) / (rhs.m_re * rhs.m_re + rhs.m_im * rhs.m_im);
        T _im = (rhs.m_re * this->m_im - this->m_re * rhs.m_im) / (rhs.m_re * rhs.m_re + rhs.m_im * rhs.m_im);
        this->m_im = _im;
        this->m_re = _re;
        return *this;
    }
    template <class Tp>
    TComplex& operator+=(const Tp& rhs)
    {
        this->m_re += rhs;
        return *this;
    }
    template <class Tp>
    TComplex& operator-=(const Tp& rhs)
    {
        this->m_re -= rhs;
        return *this;
    }
    template <class Tp>
    TComplex& operator*=(const Tp& rhs)
    {
        this->m_re *= rhs;
        this->m_im *= rhs;
        return *this;
    }
    template <class Tp>
    TComplex& operator/=(const Tp& rhs)
    {
        this->m_re /= rhs;
        this->m_im /= rhs;
        return *this;
    }
    template <class Tp>
    TComplex operator+(const Tp& rhs)
    {
        TComplex<T> _res = *this;
        _res += rhs;
        return _res;
    }
    template <class Tp>
    TComplex operator-(const Tp& rhs)
    {
        TComplex<T> _res = *this;
        _res -= rhs;
        return _res;
    }
    template <class Tp>
    TComplex operator*(const Tp& rhs)
    {
        TComplex<T> _res = *this;
        _res *= rhs;
        return _res;
    }
    template <class Tp>
    TComplex operator/(const Tp& rhs)
    {
        TComplex<T> _res = *this;
        _res /= rhs;
        return _res;
    }
    ~TComplex() = default;
    T real()
    {
        return m_re;
    }
    T imag()
    {
        return m_im;
    }
    void real(T _real)
    {
        this->m_re = _real;
    }
    void imag(T _imag)
    {
        this->m_im = _imag;
    }
    friend std::ostream& operator<< (std::ostream &out, const TComplex<T> &c)
    {
        out<<"( " << c.m_re << " , " << c.m_im << " )";
        return out;
    }
private:
    T m_re;
    T m_im;
};

template <class Tp>
inline TComplex<Tp> operator+(const Tp& x, TComplex<Tp> y)
{
    TComplex<Tp> _res = y;
    _res += x;
    return _res;
}
template <class Tp>
inline TComplex<Tp> operator-(const Tp& x, TComplex<Tp> y)
{
    TComplex<Tp> _res(x, -y.imag());
    _res -= y.real();
    return _res;
}
template <class Tp>
inline TComplex<Tp> operator*(const Tp& x, TComplex<Tp> y)
{
    TComplex<Tp> _res = y;
    _res *= x;
    return _res;
}
template <class Tp>
inline TComplex<Tp> operator/(const Tp& x, TComplex<Tp> y)
{
    TComplex<Tp> _res = x;
    _res /= y;
    return _res;
}

template<class T>
TComplex<T> sqrt(TComplex<T> _val)
{
    T _a = _val.real();
    T _b = _val.imag();
    T sgn = (_b >= 0) ? 1.0 : -1.0;
    T _re = std::sqrt((std::sqrt(_a * _a + _b * _b) + _a) / 2.0);
    T _im = std::sqrt(sgn * (std::sqrt(_a * _a + _b * _b) - _a) / 2.0);
    return TComplex<T>(_re, _im);
}

#endif // TCOMPLEX_H
