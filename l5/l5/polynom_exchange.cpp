#include "polynom_exchange.h"

IUdpExchange::IUdpExchange(QObject *parent) : QObject(parent)
{
    m_running = false;
}

IUdpExchange::~IUdpExchange()
{
    m_socket.close();
}

bool IUdpExchange::running() const
{
    return m_running;
}



PolynomialServer::PolynomialServer(QObject *parent) : IUdpExchange(parent)
{

}

bool PolynomialServer::start()
{
    bool rc = m_socket.bind(QHostAddress::LocalHost, 1337);
    if (!rc) return false;
    connect(&m_socket, &QUdpSocket::readyRead, this, &PolynomialServer::handleConnection);
    m_running = true;
    return true;

}

bool PolynomialServer::stop()
{
    m_socket.close();
    m_running = false;
    return true;
}

void PolynomialServer::handleConnection()
{
    while (m_socket.hasPendingDatagrams()) {
        auto dgram = m_socket.receiveDatagram();
        if (dgram.data().size() != sizeof (polynom_datagram_task_t)) {
            std::cout<<"Wrong packet size: " << dgram.data().size() << std::endl;
            continue;
        }
        polynom_datagram_task_t recv_polynom;
        memcpy(&recv_polynom, dgram.data().data(), sizeof recv_polynom);
        std::array<TComplex<double>,3> task;
        task[0].real(recv_polynom.a_re);
        task[0].imag(recv_polynom.a_im);
        task[1].real(recv_polynom.b_re);
        task[1].imag(recv_polynom.b_im);
        task[2].real(recv_polynom.c_re);
        task[2].imag(recv_polynom.c_im);
        std::cout<<"Received bytes: "<<dgram.data().size()<<std::endl;
        for (const auto &t : task) std::cout<<t<<" ";
        std::cout<<std::endl;
        polynom<TComplex<double>> p(task[0], task[1], task[2]);
        std::array<TComplex<double>,2> roots;
        p.solve_complex(roots);
        polynom_datagram_result_t send_polynom;
        send_polynom.x1_im = roots[0].imag();
        send_polynom.x1_re = roots[0].real();
        send_polynom.x2_im = roots[1].imag();
        send_polynom.x2_re = roots[1].real();
        QByteArray send_data;
        send_data.resize(sizeof send_polynom);
        memcpy(send_data.data(), &send_polynom, sizeof send_polynom);
        QNetworkDatagram send_dgram(send_data, dgram.senderAddress(), dgram.senderPort());
        m_socket.writeDatagram(send_dgram);
    }
}

PolynomialClient::PolynomialClient(QObject *parent) : IUdpExchange(parent)
{

}

std::vector<TComplex<double>> PolynomialClient::sendPolynom(std::array<TComplex<double>, 3> &poly)
{
    polynom_datagram_task_t task;
    task.a_re = poly[0].real();
    task.a_im = poly[0].imag();
    task.b_re = poly[1].real();
    task.b_im = poly[1].imag();
    task.c_re = poly[2].real();
    task.c_im = poly[2].imag();
    QByteArray send_data;
    send_data.resize(sizeof task);
    memcpy(send_data.data(), &task, sizeof task);
    QNetworkDatagram send_dgram(send_data, QHostAddress::LocalHost, 1337);
    assert(m_socket.writeDatagram(send_dgram) == sizeof task);
    if (m_socket.waitForReadyRead(5000)) {
        auto dgram = m_socket.receiveDatagram();
        if (!dgram.isValid()) return std::vector<TComplex<double>>();
        polynom_datagram_result_t res;
        memcpy(&res, dgram.data().constData(), sizeof res);
        return std::vector<TComplex<double>>({TComplex<double>(res.x1_re, res.x1_im), TComplex<double>(res.x2_re, res.x2_im)});
    }
    else {
        return std::vector<TComplex<double>>();
    }
}
