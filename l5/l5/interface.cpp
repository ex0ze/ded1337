#include "interface.h"
#include "ui_interface.h"

PolynomGui::PolynomGui(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::interface)
{
    ui->setupUi(this);
    QLocale::setDefault(QLocale(QLocale::C));
    polynom_server = nullptr;
    connect(ui->calculate_btn, &QPushButton::pressed, this, &PolynomGui::calculateRemote);
    connect(ui->action_reset, &QAction::triggered, this, &PolynomGui::resetCFs);
    setValidators();
}

PolynomGui::~PolynomGui()
{
    delete ui;
    if (polynom_server != nullptr) {
        polynom_server->kill();
        delete polynom_server;
    }
}

void PolynomGui::calculateRemote()
{
    std::array<TComplex<double>, 3> input;
    bool ok = true;
    input[0].real(ui->re_x2->text().toDouble(&ok));
    input[0].imag(ui->im_x2->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^2");
        return;
    }
    input[1].real(ui->re_x1->text().toDouble(&ok));
    input[1].imag(ui->im_x1->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^1");
        return;
    }
    input[2].real(ui->re_x0->text().toDouble(&ok));
    input[2].imag(ui->im_x0->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^0");
        return;
    }
    PolynomialClient cli(this);
    auto res = cli.sendPolynom(input);
    if (res[0].imag() == 0 && res[1].imag() == 0) {
        ui->res_x1->setText(QString::number(res[0].real()));
        ui->res_x2->setText(QString::number(res[1].real()));
    }
    else {
        ui->res_x1->setText(QString("re: %1 ; im: %2").arg(res[0].real()).arg(res[0].imag()));
        ui->res_x2->setText(QString("re: %1 ; im: %2").arg(res[1].real()).arg(res[1].imag()));
    }
}

void PolynomGui::resetCFs()
{
    ui->re_x0->setText("0");
    ui->im_x0->setText("0");
    ui->re_x1->setText("0");
    ui->im_x1->setText("0");
    ui->re_x2->setText("0");
    ui->im_x2->setText("0");

    ui->res_x1->clear();
    ui->res_x2->clear();
}

void PolynomGui::setValidators()
{
    ui->re_x0->setValidator(new QDoubleValidator(this));
    ui->im_x0->setValidator(new QDoubleValidator(this));
    ui->re_x1->setValidator(new QDoubleValidator(this));
    ui->im_x1->setValidator(new QDoubleValidator(this));
    ui->re_x2->setValidator(new QDoubleValidator(this));
    ui->im_x2->setValidator(new QDoubleValidator(this));
}

