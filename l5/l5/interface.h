#ifndef INTERFACE_H
#define INTERFACE_H

#include <QMainWindow>
#include <QMessageBox>
#include <QValidator>
#include <QProcess>
#include "polynom.h"
#include "polynom_exchange.h"
#include "tcomplex.h"

QT_BEGIN_NAMESPACE
namespace Ui { class interface; }
QT_END_NAMESPACE

class PolynomGui : public QMainWindow
{
    Q_OBJECT

public:
    PolynomGui(QWidget *parent = nullptr);
    ~PolynomGui();

private slots:
    void calculateRemote();
    void resetCFs();
    void setValidators();

private:
    Ui::interface *ui;
    QProcess* polynom_server;
};
#endif // INTERFACE_H
