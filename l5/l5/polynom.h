#ifndef POLYNOM_H
#define POLYNOM_H

#include <vector>
#include <array>
#include <cmath>
#include "tcomplex.h"
template <class T>
class polynom
{
public:
    polynom() : m_a(0), m_b(0), m_c(0)
    {
        static_assert (std::is_arithmetic<T>::value || std::is_same<TComplex<double>,typename std::decay<T>::type>::value ,
                "Value should be integral or complex<double> type");
    }
    polynom(T a, T b, T c) : m_a(a), m_b(b), m_c(c)
    {
        static_assert (std::is_arithmetic<T>::value || std::is_same<TComplex<double>,typename std::decay<T>::type>::value ,
                "Value should be integral or complex<double> type");
    }
    int solve(std::array<T,2>& roots)
    {
        static_assert (std::is_arithmetic<T>::value,
                "Solve() function must be only used with arithmetic types" );
        T d = m_b * m_b - 4.0 * m_a * m_c;
        if (d < 0.0) return 0;
        else if (d == 0.0) {
            roots[0] = -m_b / (2.0 * m_a);
            return 1;
        }
        else {
            roots[0] = (-m_b - std::sqrt(d)) / (2.0 * m_a);
            roots[1] = (-m_b + std::sqrt(d)) / (2.0 * m_a);
            return 2;
        }
    }
    template <class Q = T>
    typename std::enable_if<std::is_same<TComplex<double>, typename std::decay<Q>::type>::value, void>::type
    solve_complex(std::array<T,2>& roots)
    {
        T sqrt_d = sqrt(m_b * m_b - 4.0 * m_a * m_c);
        roots[0] = (-m_b - sqrt_d) / (2.0 * m_a);
        roots[1] = (-m_b + sqrt_d) / (2.0 * m_a);
    }
private:
    T m_a;
    T m_b;
    T m_c;
};

#endif // POLYNOM_H
