#include <QCoreApplication>
#include "../../libs/polynom_exchange.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    PolynomialServer srv;
    assert(srv.start());
    return a.exec();
}
