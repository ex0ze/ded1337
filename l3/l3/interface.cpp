#include "interface.h"
#include "ui_interface.h"

#define SERVER_PROCESS_NAME "C:\\Users\\boyne\\source\\Qt\\chaotic-evil\\build-l4_server-Desktop_Qt_5_13_1_MinGW_64_bit-Debug\\debug\\l4_server.exe"

PolynomGui::PolynomGui(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::interface)
{
    ui->setupUi(this);
    QLocale::setDefault(QLocale(QLocale::C));
    polynom_server = nullptr;
    connect(ui->calculate_btn, &QPushButton::pressed, this, &PolynomGui::calculateLocal);
    connect(ui->action_reset, &QAction::triggered, this, &PolynomGui::resetCFs);
    connect(ui->action_remote, &QAction::triggered, [&](bool isChecked){
       if (isChecked) {
           disconnect(ui->calculate_btn, &QPushButton::pressed, this, &PolynomGui::calculateLocal);
           connect(ui->calculate_btn, &QPushButton::pressed, this, &PolynomGui::calculateRemote);
       }
       else {
           disconnect(ui->calculate_btn, &QPushButton::pressed, this, &PolynomGui::calculateRemote);
           connect(ui->calculate_btn, &QPushButton::pressed, this, &PolynomGui::calculateRemote);
       }
    });
    setValidators();
}

PolynomGui::~PolynomGui()
{
    delete ui;
    if (polynom_server != nullptr) {
        polynom_server->kill();
        delete polynom_server;
    }
}

void PolynomGui::calculateLocal()
{
    TComplex input[3];
    bool ok = true;
    input[0].real(ui->re_x2->text().toDouble(&ok));
    input[0].imag(ui->im_x2->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^2");
        return;
    }
    input[1].real(ui->re_x1->text().toDouble(&ok));
    input[1].imag(ui->im_x1->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^1");
        return;
    }
    input[2].real(ui->re_x0->text().toDouble(&ok));
    input[2].imag(ui->im_x0->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^0");
        return;
    }
    polynom _polynom(input[0], input[1], input[2]);
    TComplex roots[2];
    _polynom.solve(roots);
    ui->res_x1->setText(QString("re: %1 ; im: %2").arg(roots[0].real()).arg(roots[0].imag()));
    ui->res_x2->setText(QString("re: %1 ; im: %2").arg(roots[1].real()).arg(roots[1].imag()));
}

void PolynomGui::calculateRemote()
{
    TComplex input[3];
    bool ok = true;
    input[0].real(ui->re_x2->text().toDouble(&ok));
    input[0].imag(ui->im_x2->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^2");
        return;
    }
    input[1].real(ui->re_x1->text().toDouble(&ok));
    input[1].imag(ui->im_x1->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^1");
        return;
    }
    input[2].real(ui->re_x0->text().toDouble(&ok));
    input[2].imag(ui->im_x0->text().toDouble(&ok));
    if (!ok) {
        QMessageBox::critical(this, "Ошибка ввода", "Неверно введены коэффициенты при X^0");
        return;
    }
    PolynomialClient cli(this);
    TComplex res[2];
    int rc = cli.sendPolynom(input, res);
    if (rc == -1) {
        QMessageBox::critical(this, "Error", "Lol");
        return;
    }
    ui->res_x1->setText(QString("re: %1 ; im: %2").arg(res[0].real()).arg(res[0].imag()));
    ui->res_x2->setText(QString("re: %1 ; im: %2").arg(res[1].real()).arg(res[1].imag()));
}

void PolynomGui::resetCFs()
{
    ui->re_x0->setText("0");
    ui->im_x0->setText("0");
    ui->re_x1->setText("0");
    ui->im_x1->setText("0");
    ui->re_x2->setText("0");
    ui->im_x2->setText("0");

    ui->res_x1->clear();
    ui->res_x2->clear();
}

void PolynomGui::setValidators()
{
    ui->re_x0->setValidator(new QDoubleValidator(this));
    ui->im_x0->setValidator(new QDoubleValidator(this));
    ui->re_x1->setValidator(new QDoubleValidator(this));
    ui->im_x1->setValidator(new QDoubleValidator(this));
    ui->re_x2->setValidator(new QDoubleValidator(this));
    ui->im_x2->setValidator(new QDoubleValidator(this));
}

