#ifndef INTERFACE_H
#define INTERFACE_H

#include <QMainWindow>
#include <QMessageBox>
#include <QValidator>
#include <QProcess>
#include "../../libs/polynom.h"
#include "../../libs/polynom_exchange.h"
#include "../../libs/tcomplex.h"

QT_BEGIN_NAMESPACE
namespace Ui { class interface; }
QT_END_NAMESPACE

class PolynomGui : public QMainWindow
{
    Q_OBJECT

public:
    PolynomGui(QWidget *parent = nullptr);
    ~PolynomGui();

private slots:
    void calculateLocal();
    void calculateRemote();
    void resetCFs();
    void setValidators();

private:
    Ui::interface *ui;
    QProcess* polynom_server;
};
#endif // INTERFACE_H
